import React from "react"

export default class Footer extends React.Component{
    render(){
       return(
           <footer className="footer">
           <div className="content">
               <h1>Powered by WiredCarbon</h1>
               <h1>Copyright 2021</h1>
           </div>
       </footer>);
    }
}
