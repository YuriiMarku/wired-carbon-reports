export default class ServerConfig{
    static devMode: boolean | null = false; // true for development; false for production; null for staging.
    static prodUrl: string = 'https://apps.wiredcarbon.com/';
    static testUrl: string = 'https://test.apps.wiredcarbon.com/';
    static stagingUrl: string = 'https://staging.apps.wiredcarbon.com/';

    static getUrl(): string{
        switch(ServerConfig.devMode){
            case true:
                return ServerConfig.testUrl;
            case false:
                console.log = () => {}; // Disable console.log
                return ServerConfig.prodUrl;
            default:
                console.log = () => {}; // Disable console.log
                return ServerConfig.stagingUrl;
        }
    }
}
