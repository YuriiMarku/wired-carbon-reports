
export default class ReportModel {
    name;
    id;
    startDate;
    endDate;
    createdDate;
    status;

    constructor(
        name = '',
        id = 0,
        startDate = new Date(),
        endDate = new Date(),
        createdDate = new Date(),
        status = '',
    ){
        this.name = name;
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdDate = createdDate;
        this.status = status;
    }
}
